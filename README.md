# Fasten Your Seatbelts (FYS) <2021>

The assignment for project FYS for the Cyber Security learning route of 'Amsterdam University of Applied Sciences' is:

Design and implement a system that enables Internet access for mobile devices (via WIFI) from an aircraft. Before access to the Internet is granted, a passenger must first register with his name and ticket number. This is achieved by sending the passenger to a so-called 'landing page' of a Captive Portal. Only after successful registration will access to the Internet be possible. Naturally, the system must be well protected against misuse and malicious intent.

## Documentation

For more insight about this product, refer to the documents folder.